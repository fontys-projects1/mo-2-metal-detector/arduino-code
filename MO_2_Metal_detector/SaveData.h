// SaveData.h
#pragma once
#include <avr/eeprom.h>
#include "MainHeader.h"

// EEPROM items
const uint8_t EEPROM_DATA_SIZE = sizeof(uint16_t);
enum EEPROM_DataLocation
{
	EEPROM_THRESHOLD_START = 0,
	EEPROM_Threshold_0,
	EEPROM_Threshold_1,
	EEPROM_Threshold_2,
	EEPROM_Threshold_3,
	EEPROM_Threshold_4,
	EEPROM_X_OFFSET_START,
	EEPROM_X_Offset_0,
	EEPROM_X_Offset_1,
	EEPROM_X_Offset_2,
	EEPROM_X_Offset_3,
	EEPROM_X_Offset_4,
	EEPROM_MIN_START,
	EEPROM_Min_0,
	EEPROM_Min_1,
	EEPROM_Min_2,
	EEPROM_Min_3,
	EEPROM_Min_4,
	EEPROM_MAX_START,
	EEPROM_Max_0,
	EEPROM_Max_1,
	EEPROM_Max_2,
	EEPROM_Max_3,
	EEPROM_Max_4,
	EEPROM_TUNING_VALUE
};


// read all the EEPROM data and write it to the metal detector object
void readAllEEPROMData(MetalDetector **metalDetector);

// write data to EEPROM
// needs:
// - dataID from EEPROM_DataLocation
// - data to write(uint16_t)
void writeToEEPROM(uint8_t dataID, uint8_t dataID_offset, uint16_t data);

// write the tuning value to EEPROM
void writeTuningValue(int16_t tuningValue);

// read the tuning value from EEPROM
int16_t getTuningValue();
