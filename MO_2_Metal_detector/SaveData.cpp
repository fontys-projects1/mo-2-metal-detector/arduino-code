// 
// 
// 

#include "SaveData.h"

uint16_t readFromEEPROM(uint8_t detectorID, uint8_t dataOffset)
{
	uint16_t data = 0;
	uint8_t dataID = (detectorID * EEPROM_DATA_SIZE) + (dataOffset * EEPROM_DATA_SIZE);
	data = eeprom_read_word((uint16_t*)dataID);
	while (!eeprom_is_ready());
	return data;
}

// read all the EEPROM data and write it to the metal detector object
void readAllEEPROMData(MetalDetector **metalDetector)
{
	for (uint8_t i = 0; i < DetectorCount; i++)
	{
		metalDetector[i]->setThreshold(readFromEEPROM(i, EEPROM_THRESHOLD_START));
		metalDetector[i]->setX_offset(readFromEEPROM(i, EEPROM_X_OFFSET_START));
		metalDetector[i]->setZeroLevel(readFromEEPROM(i, EEPROM_MIN_START), true);
		metalDetector[i]->setMaxLevel(readFromEEPROM(i, EEPROM_MAX_START), true);

	}
}

// write data to EEPROM
// needs:
// - dataID from EEPROM_DataLocation
// - data to write(uint16_t)
void writeToEEPROM(uint8_t detectorID, uint8_t dataID_offset, uint16_t data)
{
	uint8_t dataLocation = (detectorID * EEPROM_DATA_SIZE) + (dataID_offset * EEPROM_DATA_SIZE);
	eeprom_update_word((uint16_t*)dataLocation, data);
	while (!eeprom_is_ready());
}

// write the tuning value to EEPROM
void writeTuningValue(int16_t tuningValue)
{
	writeToEEPROM(0, EEPROM_TUNING_VALUE, (uint16_t)tuningValue);
}

// read the tuning value from EEPROM
int16_t getTuningValue()
{
	uint16_t value = readFromEEPROM(0, EEPROM_TUNING_VALUE);
	return (int16_t)value;
}
