#include "MetalDetector.h"

// blobal setting storage
bool MetalDetector::s_globalStetting = false;   		// enable global settings
uint16_t MetalDetector::s_threshold = 0;				// global threshold
bool MetalDetector::s_valueCalibration = false;			// global value calibration

MetalDetector::MetalDetector() {} // pre-constructor

// Constructor, set the pin number
// needs:
// - pin number
// - x offset
// - enable global settings
// - boolean if it's needs to set the low level automatic
// - bytemapping setting
MetalDetector::MetalDetector(uint8_t pin, uint16_t xOffset, bool enableGlobalSettings, bool autoCalibrate, bool byteMapping)
{
	this->pin = pin;
	this->xOffset = xOffset;
	this->byteMapping = byteMapping;
	this->maxLevel = byteMapping ? 255 : 1023;		// set max level to 255 when byte mapping is enabled else 1023
	this->autoCalibrate = autoCalibrate;
	this->setGlobalSettings(enableGlobalSettings);  // enable global settings
	pinMode(pin, INPUT);							// set pin to input
}

MetalDetector::~MetalDetector() {}	// destructor

// read ADC and avarage the reading
void MetalDetector::readADC()		
{
	// read ADC and store the value storedValueCount times
	if (storedValueCount < filterCount)
	{
		// when byte mapping is enabled convert the value to 8 bit
		if (byteMapping)
		{
			// convert to 8 bit
			storedValue += map(analogRead(pin), 0, 1023, 0, 255);
		}
		// when byte mapping is disabled store the value as default (10 bit)
		else 
		{
			storedValue += analogRead(pin);
		}

		// increase the stored value count
		storedValueCount++;
	}
	// when storedValueCount is equal to filterCount
	else
	{
		rawValue = storedValue / filterCount; // calculate the avarage value
		int16_t value = 0;

		// when value calibration is enabled
		if (*this->p_valueCalibration)
		{
			// map the value to 0 - 255 or 0 - 1023 (depends on byte mapping)
			value = map(rawValue, minLevel, maxLevel, 0, byteMapping ? 255 : 1023);
			
			// check if the value is in the range
			// when the value is lower than 0 set the value to 0
			if (value < 0) value = 0;
			// when the value is higher than 255 or 1023 set the value to 255 or 1023 (depends on byte mapping)
			else if (value > (byteMapping ? 255 : 1023)) value = byteMapping ? 255 : 1023;
		}
		// when value calibration is disabled
		else
		{
			value = rawValue;   	// store the value temporary
			// check if the value is higher than the min level
			if (value > minLevel)
			{
				// substract the min level
				value -= minLevel;
			}
			// when the value is lower than the min level set the value to 0
			else value = 0;
		}
		
		// auto calibrate can only be active on the first run
		if (autoCalibrate)
		{
			// set the min level to the current value
			minLevel = value;
			// disable auto calibrate
			autoCalibrate = false;
		}
		// when the init auto calibrate is disabled, set detect value to the current value
		else
		{
			detectValue = value;
		}

		// reset the stored value and the stored value count
		storedValue = 0;
		storedValueCount = 0;
	}
}

// needs to be called constantly
// returns the current value
uint16_t MetalDetector::update()
{
	// read ADC
	this->readADC();
	
	// check if metal is detected
	if (detectValue > *this->p_threshiold)
	{
		// metal is detected
		if (!isDetected)
		{
			startTime = millis();		// save the start time
			isDetected = true;			// set is detected to true
			wasDetected = true;		 	// set was detected to true
		}
	}
	// when metal is not detected
	else
	{
		// metal is not detected
		if (isDetected)
		{
			endTime = millis(); 		// save the end time
			isDetected = false;			// set is detected to false
		}
	}

	// set the new pointers when the global setting is changed
	if (s_globalStetting != old_globalStetting)
	{
		old_globalStetting = s_globalStetting; 	 // save the new global setting
		setPointers();							 // set the new pointers
	}

	return detectValue; 		// return the current value
}

// set the right prointers 
void MetalDetector::setPointers()
{
	// set the right pointer for the threshold
	// set the right pointer fot the value mapping setting
	if (s_globalStetting)	// when global setting is enabled
	{
		p_threshiold = &s_threshold;
		p_valueCalibration = &s_valueCalibration;
	}
	// when global setting is disabled
	else
	{
		p_threshiold = &this->threshold;
		p_valueCalibration = &this->valueCalibration;
	}
}

// set zero level
// apply the given value when given else use the current value
void MetalDetector::setZeroLevel(uint16_t minLevel = 0, bool overRide = false)
{
	// when the given value is higher than 0 set the min level to the given value
	if (minLevel > 0 || overRide)
	{
		this->minLevel = minLevel;
	}
	// when the given value is lower than 0 set the min level to the current value
	else
	{
		this->minLevel = rawValue;
	}
}

// get the zero level
uint16_t MetalDetector::getZeroLevel()
{
	return minLevel;
}

// set max level
// apply the given value when given else use the current value
void MetalDetector::setMaxLevel(uint16_t maxLevel = 0, bool overRide = false)
{
	// when the given value is higher than 0 set the max level to the given value
	if (maxLevel > 0 || overRide)
	{
		this->maxLevel = maxLevel;
	}
	// when the given value is lower than 0 set the max level to the current value
	else
	{
		this->maxLevel = rawValue;
	}
}

// get the max value
uint16_t MetalDetector::getMaxLevel()
{
	return maxLevel;
}

// set threshold
void MetalDetector::setThreshold(uint16_t threshold)
{
	*this->p_threshiold = threshold;
}

// is metal detected
bool MetalDetector::isActive()
{
	return isDetected;
}

// time in milliseconds when metal was detected
uint32_t MetalDetector::getTimeStart()
{
	return startTime;
}

// time in milliseconds when metal was not detected
uint32_t MetalDetector::getTimeEnd()
{
	return endTime;
}

// total time in milliseconds when metal was detected
uint32_t MetalDetector::getTimeDetected()
{
	return endTime > startTime ? endTime - startTime : 0;
}

// when enabled, active value range will be between the min and max value, else the raw value will used
void MetalDetector::setValueCalibration(bool enable)
{
	*p_valueCalibration = enable;
}

// define if the globale setting will be enabled
// when enabled, the threshold and maping state will be shared beween the objects
void MetalDetector::setGlobalSettings(bool enable)
{
	s_globalStetting = enable;

	// set the right pointers
	setPointers();
}

// get the value calibration state
bool MetalDetector::getValueCalibrationState()
{
	return *this->p_valueCalibration;
}

// get the global setting state
bool MetalDetector::getGlobalSettingsState()
{
	return s_globalStetting;
}

// get the threshold
uint16_t MetalDetector::getThreshold()
{
	return *this->p_threshiold;
}

// set the byte mapping state
void MetalDetector::set8bitReading(bool enable)
{
	this->byteMapping = enable;
}

// get the byte mapping state
bool MetalDetector::get8bitReadingState()
{
	return this->byteMapping;
}

// get the raw value
uint16_t MetalDetector::getValue()
{
	return detectValue;
}

// get the x offset
uint16_t MetalDetector::getX_offset()
{
	return xOffset;
}

// set the x offset
void MetalDetector::setX_offset(uint16_t offset)
{
	this->xOffset = offset;
}

// get if the metal detector was active
// when reset is true, the was detected state will be reset
bool MetalDetector::wasActive(bool reset = false)
{
	bool tmp = wasDetected;
	if (reset) wasDetected = false;
	return tmp;
}