#pragma once
#include <Arduino.h>
#include "MetalDetector.h"
#include "SaveData.h"
#include "Communitcation.h"

const bool ENABLE_8BIT_READING = false;			// enable 8 bit reading
const bool ENABLE_AUTO_SET_LOW_LEVEL = false;	// enable auto set low level
const bool ENABLE_GLOBAL_SETTINGS = false;		// enable global settings
const uint32_t UPDATE_INTERVAL = 200;			// update interval in ms
const uint16_t THRESHOLD = 1010;				// threshold for detecting metal
const uint32_t DISTANCE = 154;					// distance between the front and back snesor in mm
const uint8_t X_OFFSET = 75;					// x offset in mm

// the detectors
enum Detectors
{
	Detector1 = 0,
	Detector2,
	Detector3,
	Detector4,
	Detector5,
	DetectorCount
};

// the pins of the detectors
const uint8_t pins[Detectors::DetectorCount] =
{
	A0,
	A1,
	A2,
	A3,
	A4
};

// communication message: Start character + Command + data length + data 
// Start character: 0x23 
const char START_CHAR = '#';		// 0b0010 0011
const char START_CHAR_STATE = '@';	// 0b0100 0000

// communication send commands
enum CommunicationSendCommands
{
	SendTimeOfMeagurment = 0,
	SendLength,
	SendLength_2,
	SendLength_Average,
	SendWidthMin,
	SendWidthMax,
	SendSpeed,
	SendThreshold,
	SendZeroLevel,
	SendMaxLevel,
	SendState,
	SendGlobalSettings,
	SendX_offset,
	SendTuning
};

// communication set commands
enum CommunicationSetCommands
{
	SetThreshold = 0,
	SetZeroLevel,
	SetMaxLevel,
	SetX_offset,
	SetGlobalSettings,
	GetThreshold,
	GetX_offset,
	GetTuning,
	SetTuning
};

// status of the calculations
enum CalculationState
{
	CalculationState_Idle = 0,
	CalculationState_speed,
	CalculationState_length,
	CalculationState_length_2,
	CalculationState_width,
	CalculationState_sendData
};

// the data struct for a object
struct Object
{
	uint16_t length;
	uint16_t length_2;
	uint16_t widthMin;
	uint16_t widthMax;
	uint32_t timeOfMeagurment;
	uint16_t speed;
};

