/*
 Name:		MO_2_Metal_detector.ino
 Created:	12/5/2022 2:31:07 PM
 Author:	Tommy
*/
#include "mainHeader.h"
#include "calculations.h"

// create the detector array
MetalDetector** metalDetector = new MetalDetector * [DetectorCount];

// create a object to store the data of the object
struct Object object;

// the setup function runs once when you press reset or power the board
void setup()
{
	Serial.begin(115200);
	for (uint8_t i = 0; i < DetectorCount; i++)
	{
		metalDetector[i] = new MetalDetector(pins[i], X_OFFSET, ENABLE_GLOBAL_SETTINGS, ENABLE_AUTO_SET_LOW_LEVEL, ENABLE_8BIT_READING);
		metalDetector[i]->setValueCalibration(true);
		metalDetector[i]->setThreshold(THRESHOLD);
	}
	
	readAllEEPROMData(metalDetector);	
}

// the loop function runs over and over again until power down or reset
void loop()
{
	static uint32_t lastTime = 0;
	static uint16_t* detectorValues = new uint16_t[Detectors::DetectorCount];
	static uint8_t state = CalculationState_Idle;

	// check if there is serial data available, then read it
	if (Serial.available() > 0) readSerialData(metalDetector);
	 
	// update the detector and save the value
	for (uint8_t i = 0; i < DetectorCount; i++)
	{
		detectorValues[i] = metalDetector[i]->update();
	}

	// main state machine
	switch (state)
	{
	case CalculationState_Idle:
		// next stage
		state = CalculationState_speed;
		break;

	case CalculationState_speed:
		// calculate the speed
		if (calculateSpeed(metalDetector, &object.speed))
		{
			// save the time of measurment
			object.timeOfMeagurment = metalDetector[Detector1]->getTimeStart();
			// next stage
			state = CalculationState_length;
		}
		break;

	case CalculationState_length:
		// calculate the length
		if (calculateLength(metalDetector, object.speed, &object.length, Detector1))
		{
			// next stage
			state = CalculationState_length_2;
		}
		break;

	case CalculationState_length_2:
		// calculate the length of the second detector
		if (calculateLength(metalDetector, object.speed, &object.length_2, Detector2))
		{
			// next stage
			state = CalculationState_width;
		}
		break;

	case CalculationState_width:
		// calculate the width
		if (calculateWidth(metalDetector, &object))
		{
			// next stage
			state = CalculationState_sendData;
		}
		break;

	case CalculationState_sendData:
		// send the data
		// send speed
		sendData(CommunicationSendCommands::SendSpeed, &object.speed, sizeof(object.speed));
		// send length 1
		sendData(CommunicationSendCommands::SendLength, &object.length, sizeof(object.length));
		// send length 2
		sendData(CommunicationSendCommands::SendLength_2, &object.length_2, sizeof(object.length_2));
		// send avrage length
		uint16_t length = (object.length + object.length_2) / 2;
		sendData(CommunicationSendCommands::SendLength_Average, &length, sizeof(length));
		// send widthMin
		sendData(CommunicationSendCommands::SendWidthMin, &object.widthMin, sizeof(object.widthMin));
		// send widthMax
		sendData(CommunicationSendCommands::SendWidthMax, &object.widthMax, sizeof(object.widthMax));
		// send the time of measgurment
		sendData(CommunicationSendCommands::SendTimeOfMeagurment, &object.timeOfMeagurment, sizeof(object.timeOfMeagurment));

		// reset wasDetected
		for (uint8_t i = 0; i < DetectorCount; i++)
		{
			metalDetector[i]->wasActive(true);
		}
		// next stage
		state = CalculationState_Idle;
		break;

	default:
		state = CalculationState_Idle;
		break;
	}

	// send the state when the state has changed
	static uint8_t lastState;
	if (lastState != state)
	{
		sendData(CommunicationSendCommands::SendState, (uint8_t*)&state, sizeof(state));
		lastState = state;
	}

	// send the detector values every UPDATE_INTERVAL
	if (millis() - lastTime > UPDATE_INTERVAL)
	{
		lastTime = millis();
		for (uint8_t i = 0; i < DetectorCount; i++)
		{
			sendData(i, &detectorValues[i], sizeof(detectorValues[i]), true, metalDetector[i]->isActive());
		}
	}
}

