// calculations.h
#pragma once
#include "mainHeader.h"


// calculate the speed of the object 
// needs: 
// - the metal detector object array 
// - the speed (pointer)
bool calculateSpeed(MetalDetector** metalDetector, uint16_t* speed);    

// calculate the length of the object
// needs:
// - the metal detector object array
// - the speed
// - the length (pointer)
// - the detector
bool calculateLength(MetalDetector** metalDetector, uint16_t speed, uint16_t* length, Detectors detector);   

bool calculateWidth(MetalDetector** metalDetector, Object* object);