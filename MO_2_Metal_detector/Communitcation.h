// Communitcation.h
#pragma once
#include "mainHeader.h"

void sendData(uint8_t send, void* data, uint8_t size, bool sendDetectorValue = false, bool isActive = false);
void readSerialData(MetalDetector** metalDetector);