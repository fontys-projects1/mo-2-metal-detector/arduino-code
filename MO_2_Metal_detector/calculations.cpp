// 
// 
// 
#include "calculations.h"


// calculate the speed
// needs:
// - the metal detector object array
// - the speed (pointer)
bool calculateSpeed(MetalDetector** metalDetector, uint16_t* speed)
{
	bool returnState = false;
	static bool speedCalculation = true;

	// calculate the speed when detector 2 is active
	if (metalDetector[Detector2]->isActive() && speedCalculation)
	{
		// time in ms between sensors
		uint16_t timeBetweenSensors = metalDetector[Detector2]->getTimeStart() - metalDetector[Detector1]->getTimeStart();
		// calculate the speed
		//*speed = (uint32_t)((DISTANCE * 1000) / timeBetweenSensors);
		*speed = (uint32_t)(((uint32_t)metalDetector[1]->getX_offset() * 1000) / timeBetweenSensors);

		// disable the speed calculation
		speedCalculation = false;
		
		// return true when the speed is calculated
		returnState = true;
	}
	// enable the speed calculation when sensor 1 and 2 are inactive
	else if (!metalDetector[Detector2]->isActive() && !metalDetector[Detector1]->isActive())
	{
		speedCalculation = true;
	}
	return returnState;
}

// calculate the length of the object
// needs:
// - the metal detector object array
// - the speed
// - the length (pointer)
// - the detector
bool calculateLength(MetalDetector** metalDetector, uint16_t speed, uint16_t* length, Detectors detector)
{
	bool returnState = false;
	// calculate the object length
	// continu when the object is passed the detector
	if (!metalDetector[detector]->isActive() && metalDetector[detector]->wasActive())
	{
		// calculate the length of the object
		*length = (speed * metalDetector[detector]->getTimeDetected() / 1000) + getTuningValue();

		// return true when the length is calculated
		returnState = true;
	}
	return returnState;
}

bool calculateWidth(MetalDetector** metalDetector, Object* object)
{
	bool returnState = false;
	bool anyActive = false;
	
	// check if any other detector is active
	// start at 1 because 0 is the first detector
	for (uint8_t i = 1; i < DetectorCount; i++)
	{
		anyActive |= metalDetector[i]->isActive();
	} 
	
	// calculate the width of the object
	if (metalDetector[Detector2]->wasActive() && !anyActive)
	{
		// reset storage
		const uint8_t startIndex = 2;
		uint8_t activeCount = 0;
		uint8_t lastActiveModule = startIndex - 1;
		object->widthMax = 0;
		object->widthMin = 0;


		// calc the max width by adding the active detectors with there xOffset
		for (uint8_t i = startIndex; i < DetectorCount; i++)
		{
			if (metalDetector[i]->wasActive())
			{
				object->widthMin += metalDetector[i]->getX_offset();
				activeCount++;
				lastActiveModule = i;
			}
		}

		// when all the detectors where active, ther is no max known value
		if (activeCount >= DetectorCount - startIndex)
		{
			object->widthMax = object->widthMin;
		}
		// set the max width by getting the active detectors + 1
		else
		{
			for (uint8_t i = startIndex; i < DetectorCount; i++)
			{
				if (metalDetector[i]->wasActive())
				{
					object->widthMax += metalDetector[i]->getX_offset();
				}
			}
			object->widthMax += metalDetector[lastActiveModule + 1]->getX_offset();
		}
		// return true when the width is calculated
		returnState = true;
	}
	
	return returnState;
}