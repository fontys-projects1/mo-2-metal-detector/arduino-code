// 
// 
// 

#include "Communitcation.h"



// sending data: 
//  - needs send command
//  - data address
//  - data size
//  - (optional) bool to send the detector value
//  - (when sending the detector value) the detector isAvtive state
// sending data in format: Start character + Command + data length + data
// when sendDetectorValue = true, the data format is: 
// Start character + detector index + value length + detector state + value 
void sendData(uint8_t send, void* data, uint8_t size, bool sendDetectorValue = false, bool isActive = false)
{
	// create a buffer
	uint8_t settingsSize = 3;
	if (sendDetectorValue) settingsSize += 1;
	uint8_t* buffer = new uint8_t[size + settingsSize];

	// start char
	buffer[0] = sendDetectorValue ? START_CHAR_STATE : START_CHAR;
	buffer[1] = send;		// send command
	buffer[2] = size;		// data length
	if (sendDetectorValue) buffer[3] = isActive;	// detector value

	// convert the data to a char array
	for (uint8_t i = 0; i < size; i++)
	{
		buffer[i + settingsSize] = ((uint8_t*)data)[i];
	}
	// add isActive if sendDetectorValue is true


	// wait until new data can fit in the buffer
	while (Serial.availableForWrite() < size + settingsSize);

	// send the data to the Serial buffer
	for (uint8_t i = 0; i < size + settingsSize; i++)
	{
		Serial.write(buffer[i]);
	}

	// delete the buffer
	delete[] buffer;
}

// send the X_offset
void _send_xOffset(MetalDetector** metalDetector, uint8_t detectorNumber)
{
	uint32_t data = detectorNumber;
	// set the next bytes to the threshold
	data |= ((uint32_t)metalDetector[detectorNumber]->getX_offset() << 8);
	sendData(SendX_offset, &data, sizeof(data));
}

// send the threshold
void _send_threshold(MetalDetector** metalDetector, uint8_t detectorNumber)
{
	// set the first byte to the detector number
	uint32_t data = detectorNumber;
	// set the next bytes to the threshold
	data |= ((uint32_t)metalDetector[detectorNumber]->getThreshold() << 8);
	sendData(SendThreshold, &data, sizeof(data));
}

void _send_tuningValue()
{
	uint16_t val = (uint16_t)getTuningValue();
	sendData(SendTuning, &val, sizeof(uint16_t));
}

// exectue the command to detectors
// needs:
// - the metal detector object array
// - the detector number
// - the value
// - the function to execute
void executeCommand(MetalDetector** metalDetector, uint8_t detectorNumber, uint32_t value, void(*function)(MetalDetector**, uint8_t, uint32_t))
{
	// set the threshold
	if (detectorNumber == DetectorCount)
	{
		// set the threshold for all detectors
		for (uint8_t i = 0; i < DetectorCount; i++)
		{
			function(metalDetector, i, value);
		}
	}
	else
	{
		function(metalDetector, detectorNumber, value);
	}
}

// set the threshold
void _setThreshold(MetalDetector** metalDetector, uint8_t detectorNumber, uint32_t value)
{
	// set the threshold for the selected detector
	metalDetector[detectorNumber]->setThreshold(value);

	// save to eeprom
	writeToEEPROM(detectorNumber, EEPROM_THRESHOLD_START, metalDetector[detectorNumber]->getThreshold());

	// send the threshold
	_send_threshold(metalDetector, detectorNumber);
}

// set the zero level
void _setZeroLevel(MetalDetector** metalDetector, uint8_t detectorNumber, uint32_t value)
{
	// set the threshold for the selected detector
	metalDetector[detectorNumber]->setZeroLevel(value);
	// save to eeprom
	writeToEEPROM(detectorNumber, EEPROM_MIN_START, metalDetector[detectorNumber]->getZeroLevel());
}

// set the max leve
void _setMaxLevel(MetalDetector** metalDetector, uint8_t detectorNumber, uint32_t value)
{
	// set the threshold for the selected detector
	metalDetector[detectorNumber]->setMaxLevel(value);
	// save to eeprom
	writeToEEPROM(detectorNumber, EEPROM_MAX_START, metalDetector[detectorNumber]->getMaxLevel());
}

// set the X_offset
void _setX_offset(MetalDetector** metalDetector, uint8_t detectorNumber, uint32_t value)
{
	// set the threshold for the selected detector
	metalDetector[detectorNumber]->setX_offset(value);
	// save to eeprom
	writeToEEPROM(detectorNumber, EEPROM_X_OFFSET_START, metalDetector[detectorNumber]->getX_offset());
	// send data
	_send_xOffset(metalDetector, detectorNumber);
}

// set the set the global settings
void _setGlobalSettings(MetalDetector** metalDetector, uint8_t detectorNumber, uint32_t value)
{
	// set the threshold for the selected detector
	metalDetector[detectorNumber]->setGlobalSettings(value);
}

// get the threshold
void _getThreshold(MetalDetector** metalDetector, uint8_t detectorNumber, uint32_t value)
{
	// send the threshold
	_send_threshold(metalDetector, detectorNumber);
}

// get the x_offset
void _getX_offset(MetalDetector** metalDetector, uint8_t detectorNumber, uint32_t value)
{
	// send the threshold
	_send_xOffset(metalDetector, detectorNumber);
}

// process the readed serial data
void applySerialInput(MetalDetector** metalDetector, uint8_t detectorNumber, uint8_t command, uint32_t value)
{
	enum CommunicationSetCommands commandEnum = (CommunicationSetCommands)command;
	// check the command
	switch (commandEnum)
	{
	case CommunicationSetCommands::SetThreshold:
		executeCommand(metalDetector, detectorNumber, value, _setThreshold);
		break;

	case CommunicationSetCommands::SetZeroLevel:
		// set the zero level
		executeCommand(metalDetector, detectorNumber, value, _setZeroLevel);
		break;

	case CommunicationSetCommands::SetMaxLevel:
		// set the max level
		executeCommand(metalDetector, detectorNumber, value, _setMaxLevel);
		break;

	case CommunicationSetCommands::SetX_offset:
		// set the x offset
		executeCommand(metalDetector, detectorNumber, value, _setX_offset);
		break;

	case CommunicationSetCommands::SetGlobalSettings:
		// set the global settings
		executeCommand(metalDetector, detectorNumber, value, _setGlobalSettings);
		break;

	case CommunicationSetCommands::GetThreshold:
		// get the threshold
		executeCommand(metalDetector, detectorNumber, value, _getThreshold);
		break;

	case CommunicationSetCommands::GetX_offset:
		// get the x offset
		executeCommand(metalDetector, detectorNumber, value, _getX_offset);
		break;

	case CommunicationSetCommands::GetTuning:
		_send_tuningValue();
		break;

	case CommunicationSetCommands::SetTuning:
		
		writeTuningValue((int16_t)(uint16_t)value);
		_send_tuningValue();
		break;

	}
}

// serial data handler
// serial data is send in the following format:
// start char (1 byte) | command (1 byte) | data length (1 byte) | Detector number (1 byte) | data (n bytes)
void readSerialData(MetalDetector** metalDetector)
{
	// if there is data in the buffer
	if (Serial.available() >= 1)
	{
		static bool startCharDetect = false;
		static uint8_t startChar = 0;
		// check for the start char (first byte)
		if (!startCharDetect)
		{
			// read the start char
			startChar = Serial.read();
			if (startChar == START_CHAR) startCharDetect = true;
		}
		else
		{
			static bool firstByts = false;
			static uint8_t command = 0;
			static uint8_t dataLength = 0;
			static uint8_t detectorNumber = 0;
			// read the data info (next 3 bytes)
			if (!firstByts && Serial.available() > 3)
			{
				// read the command
				command = Serial.read();
				// read the data length
				dataLength = Serial.read();
				// read the detector number
				detectorNumber = Serial.read();

				// go to the next stage
				firstByts = true;
			}

			// when the data info is known and there is data avalible with the size of the data length
			if (firstByts && Serial.available() >= dataLength)
			{
				// check if the start char is correct
				if (startChar == START_CHAR)
				{
					// create a buffer
					uint8_t* buffer = new uint8_t[dataLength];

					// read the data
					for (uint8_t i = 0; i < dataLength; i++)
					{
						buffer[i] = Serial.read();
					}

					// convert char array to interger
					uint32_t value = 0;
					for (uint8_t i = 0; i < dataLength; i++)
					{
						value |= (buffer[i] << (i * 8));
					}

					// precess the readed data
					applySerialInput(metalDetector, detectorNumber, command, value);

					delete[] buffer;
					firstByts = false;
					startCharDetect = false;
					startChar = 0;
					command = 0;
					dataLength = 0;
					detectorNumber = 0;
				}
			}
		}
	}
}
