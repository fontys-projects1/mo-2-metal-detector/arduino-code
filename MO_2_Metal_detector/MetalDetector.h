#pragma once
#include <Arduino.h>

class MetalDetector
{
private:
	const uint8_t filterCount = 60; // Number of samples to average (needs to be les than 64) || 256 in 8 bit mode

	uint8_t pin;				// pin number	
	uint16_t detectValue;		
	uint16_t rawValue;			// filterd raw value
	uint16_t storedValue;
	uint8_t storedValueCount;
	uint16_t xOffset;

	uint16_t threshold = 0;
	uint16_t* p_threshiold = &threshold;
	uint16_t minLevel = 0;
	uint16_t maxLevel = 1023;

	uint32_t startTime;
	uint32_t endTime;

	bool isDetected = false;
	bool wasDetected = false;
	bool autoCalibrate;
	bool valueCalibration = false;
	bool* p_valueCalibration = &valueCalibration;
	bool old_globalStetting;
	bool byteMapping = false;

	static bool s_globalStetting;
	static uint16_t s_threshold;
	static bool s_valueCalibration;
	static bool s_8bitMapping;

	// read ADC and avarage the reading
	void readADC();

	// set pointers
	void setPointers();

public:
	// Constructor, needs:
	// - pin number 
	// - x offset
	// - enable global settings
	// - boolean if it's needs to set the low level automatic 
	// - bytemapping setting
	MetalDetector(uint8_t pin, uint16_t xOffset, bool enableGlobalSettings, bool autoCalibrate, bool byteMapping);
	MetalDetector();							// Pre-constructor
	~MetalDetector();							// Destructor

	uint32_t getTimeStart();					// time in milliseconds when metal was detected
	uint32_t getTimeEnd();						// time in milliseconds when metal was not detected
	uint32_t getTimeDetected();					// total time in milliseconds when metal was detected

	void setValueCalibration(bool enable);		// enable value mapping
	bool getValueCalibrationState();			// returns the state of valuemapping
	void setGlobalSettings(bool enable);		// enable global settings
	bool getGlobalSettingsState();				// returns the state of globalsetting
	void set8bitReading(bool enable);			// enbale 8 bit mapping
	bool get8bitReadingState();					// returns the state of 8 bit mapping

	uint16_t update();							// need to be called constantly
	void setZeroLevel(uint16_t minLevel = 0, bool overRide = false);	// set zero level
	uint16_t getZeroLevel();					// returns the zero level
	void setMaxLevel(uint16_t maxLevel = 0, bool overRide = false);	// set max level
	uint16_t getMaxLevel();						// returns the max level
	void setThreshold(uint16_t threshold);		// set threshold
	uint16_t getThreshold();					// returns the threshold value
	bool isActive();							// is metal detected
	uint16_t getValue();						// returns the reading value
	void setX_offset(uint16_t offset);			// set x offset
	uint16_t getX_offset();						// returns the x offset
	bool wasActive(bool reset = false);			// was metal detected (reset = true resets the value)
};

